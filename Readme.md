﻿# Real-time Multi-Layer rendering Project 

Author : Romain Bennert

This project aims to use a tensor display for real-time animations using OpenGL. A tensor display is a display using three layered screens with the backlight passing through the three screens before reaching the viewers eyes. Constraints are thus much different from a classical screen. Depending on the various rendering techniques used, the result from the simulated tensor display will vary greatly. 
OpenGL was used to render all the animations. The rest of the code is in c++. To build the solutions using the .cpp, .h and .frag and .vert files, CMake is used. 

There is currently 6 animation: 
MultiLayerAnimation_1.exe
MultiLayerAnimation_2.exe
MultiLayerAnimation_3.exe
These three animations each implement respectively the first, second and third rendering solution to open the three windows needed for the tensor display. (Each animation opens three screens.)

MultiLayerAnimation_animation_and_psnr_1.exe
MultiLayerAnimation_animation_and_psnr_2.exe
MultiLayerAnimation_animation_and_psnr_3.exe
These tree animations are created to compare each rendering solutions. These animations simulates one front view of the tensor display and one view with a side offset from the center of the tensor displays and computes the psnr between the simulated view and a target view. This allows to compare the different rendering techniques using an objective metric instead of just stating that one solution "looks better".

You will also find in the VideoPresentations folder 4 videos presenting the different animations and the three different rendering techniques. 

## How to run the project

You first need to install CMake which is a cross platform open-source that allows to build projects. Some IDE directly support it with extensions. I personally recommend using Visual Studio 2022 as it automatically builds the project if you import it from the git as a CMake project (you of course need to download the CMake extension for Visual Studio 2022). When the project is build, just select the right executable and launch it. 

**there is currently problems with this and the project may not run on your pc. This is the next fix coming. If you want to see the animations in action, please be patient. While waiting for the fix, you can watch the video presentations of each animation.**

## Credits 

This project is supervised by the LISA  Lab of the Université Libre de Bruxelles. If you want to see more projects around image processing and virtual reality, you can visit the lab’s website here https://lisa.polytech.ulb.be .

OpenGL is a very capricious programming interface and this project would be nowhere without the tutorials from learnopengl.com. If you ever need to work on an OpenGL application, visit their website, it probably has ressources that will help you. 

## Licence 

MIT License

Copyright (c) 2024 Bennert Romain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.