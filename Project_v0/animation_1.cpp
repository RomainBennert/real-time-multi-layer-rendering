#define GLAD_GL_IMPLEMENTATION
#include<glad/glad.h>
#define GLFW_INCLUDE_NONE
#include<GLFW/glfw3.h>

#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "math.h"

#include <vector>
#include "camera.h"
#include "objects/object.h"
#include "shaders/shader.h"

#include <chrono>
#include <thread>

const int width = 960;
const int height = 513;


GLuint compileShader(std::string shaderCode, GLenum shaderType);
GLuint compileProgram(GLuint vertexShader, GLuint fragmentShader);
void processInput(GLFWwindow* window);


#ifndef NDEBUG
void APIENTRY glDebugOutput(GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei length,
	const char* message,
	const void* userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << std::endl;
}
#endif

Camera camera(glm::vec3(0.0, 2.0, 3.0));


int main(int argc, char* argv[])
{
	std::cout << "Welcome to this Project: " << std::endl;
	std::cout << "This project aims to create a small environement to test le multi-layer screen set-up in real time \n";
	if (!glfwInit()) {
		throw std::runtime_error("Failed to initialise GLFW \n");
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifndef NDEBUG
	//create a debug context to help with Debugging
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif

	//Create the window
	std::vector<GLFWwindow*> window;
	window.resize(3);

	window[0] = glfwCreateWindow(width, height, "Real Time Multi-layer Rendering Animation 1", NULL, NULL);
	if (window[0] == NULL)
	{
		glfwTerminate();
		throw std::runtime_error("Failed to create GLFW window\n");
	}
	glfwMakeContextCurrent(window[0]);
	gladLoadGL();
	glEnable(GL_DEPTH_TEST);

	for (int layer = 1; layer < 3; layer++) {
		window[layer] = glfwCreateWindow(width, height, "Real Time Multi-layer Rendering Animation 1", NULL, window[0]);
		if (window[layer] == NULL)
		{
			glfwTerminate();
			throw std::runtime_error("Failed to create GLFW window\n");
		}
		glfwMakeContextCurrent(window[layer]);
		gladLoadGL();
		glEnable(GL_DEPTH_TEST);
	}
	
	//load openGL function
	
#ifndef NDEBUG
	int flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(glDebugOutput, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
#endif

	char fileVert[128] = PATH_TO_SHADERS "/base.vert";
	char fileFrag[128] = PATH_TO_SHADERS "/base.frag";
	Shader baseShader(fileVert, fileFrag);

	char path_dragon[] = PATH_TO_OBJECTS "/Dragon.obj";
	Object dragon(path_dragon);

	char path_cube[] = PATH_TO_OBJECTS "/tank.obj";
	Object tank(path_cube);

	char path_plane[] = PATH_TO_OBJECTS "/plane.obj";
	Object plane(path_plane);

	char path_player[] = PATH_TO_OBJECTS "/kriegsman.obj";
	Object player(path_player);

	int sceneRenders = 3;
	plane.init(sceneRenders);
	dragon.init(sceneRenders);
	player.init(sceneRenders);
	tank.init(sceneRenders);
	for (int i = 0; i < sceneRenders; i++) {
		glfwMakeContextCurrent(window[i]);
		dragon.makeObject(baseShader, false, i);
		tank.makeObject(baseShader, false, i);
		player.makeObject(baseShader, false, i);
		plane.makeObject(baseShader, false, i);
	}

	double prev = 0;
	int deltaFrame = 0;

	glm::vec3 light_pos[] = {
		glm::vec3(3.0,3.0,-4.0),
		glm::vec3(0.0,0.0,4.0),
		glm::vec3(3.0,3.0,4.0),
		glm::vec3(2.5, 2.0, -1.0)
	};

	glm::vec3 light_colors[] = {
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0)
	};

	glm::mat4 dragon_model = glm::mat4(1.0);
	glm::vec3 dragon_position = glm::vec3(-1.0, 0.0, -1.0);
	glm::vec3 dragonColour = glm::vec3(0.7, 0.1, 0.2);
	dragon_model = glm::translate(dragon_model, dragon_position);
	dragon_model = glm::scale(dragon_model, glm::vec3(0.15, 0.15, 0.15));
	dragon_model = glm::rotate(dragon_model, glm::radians((float)(45.0)), glm::vec3(0.0, 1.0, 0.0));
	glm::mat4 dragon_inverseModel = glm::transpose(glm::inverse(dragon_model));

	glm::mat4 tank_model = glm::mat4(1.0);
	glm::vec3 tankColour = glm::vec3(0.0, 0.4, 0.2);
	glm::vec3 tank_position = glm::vec3(6.0, 0.0, -7.0);
	tank_model = glm::translate(tank_model, tank_position);
	tank_model = glm::scale(tank_model, glm::vec3(0.01, 0.01, 0.01));
	tank_model = glm::rotate(tank_model, glm::radians((float)(-45.0)), glm::vec3(0.0, 1.0, 0.0));
	glm::mat4 tank_inverseModel = glm::transpose(glm::inverse(tank_model));

	glm::mat4 plane_model = glm::mat4(1.0);
	glm::vec3 planeColour = glm::vec3(0.3, 0.3, 0.3);
	glm::vec3 plane_position = glm::vec3(0.0, 0.0, 0.0);
	plane_model = glm::translate(plane_model, plane_position);
	plane_model = glm::scale(plane_model, glm::vec3(100.0, 1.0, 100.0));
	glm::mat4 plane_inverseModel = glm::transpose(glm::inverse(plane_model));

	glm::mat4 player_model = glm::mat4(1.0);
	glm::vec3 playerColour = glm::vec3(0.0, 0.03, 0.3);
	glm::vec3 player_position = glm::vec3(2.5, 0.0, -2.0);
	player_model = glm::translate(player_model, player_position);
	player_model = glm::scale(player_model, glm::vec3(1.0, 1.0, 1.0));
	player_model = glm::rotate(player_model, glm::radians((float)(-15.0)), glm::vec3(0.0, 1.0, 0.0));
	glm::mat4 player_inverseModel = glm::transpose(glm::inverse(player_model));

	glm::mat4 view;
	std::vector<glm::mat4> perspective;

	perspective.resize(4);
	view = camera.GetViewMatrix();
	perspective[0] = camera.GetProjectionMatrix();
	perspective[1] = camera.GetProjectionMatrix();
	perspective[2] = camera.GetProjectionMatrix();
	perspective[3] = camera.GetProjectionMatrix();
	float ambient = 0.2;
	float diffuse = 0.4;
	float specular = 0.4;

	float fov = 45.0;
	float ratio = 1.8;
	float minDist = 0.01;
	float maxDist = 100.0;

	glm::vec3 materialColour = glm::vec3(0.5, 0.5, 0.5);
	//Rendering

	baseShader.use();
	baseShader.setFloat("shininess", 32.0f);
	baseShader.setFloat("light.ambient_strength", ambient);
	baseShader.setFloat("light.diffuse_strength", diffuse);
	baseShader.setFloat("light.specular_strength", specular);
	baseShader.setFloat("light.constant", 0.5);
	baseShader.setFloat("light.linear", 0.15);
	baseShader.setFloat("light.quadratic", 0.03);

	auto initViewpoint = [&]() {
		glm::vec3 position1 = camera.Position;
		float dist_dragon = glm::length(position1 - dragon_position);
		float dist_tank = glm::length(position1 - tank_position);
		float dist_player = glm::length(position1 - player_position);

		float nearest_obj = glm::min(dist_dragon, dist_tank);
		nearest_obj = glm::min(dist_player, nearest_obj);
		float furthest_obj = glm::max(dist_dragon, dist_tank);
		furthest_obj = glm::max(furthest_obj, dist_player);

		view = camera.GetViewMatrix();
		perspective[0] = camera.GetProjectionMatrix(fov, ratio, minDist, nearest_obj);
		perspective[1] = camera.GetProjectionMatrix(fov, ratio, nearest_obj, furthest_obj);
		perspective[2] = camera.GetProjectionMatrix(fov, ratio, furthest_obj, maxDist);
		perspective[3] = camera.GetProjectionMatrix(fov, ratio, minDist, maxDist);
		};

	auto renderScene = [&](int layer) {

		baseShader.use();
		baseShader.setVector3f("materialColour", dragonColour);
		baseShader.setMatrix4("M", dragon_model);
		baseShader.setMatrix4("itM", dragon_inverseModel);
		baseShader.setMatrix4("V", view);
		baseShader.setMatrix4("P", perspective[layer]);
		baseShader.setVector3f("u_view_pos", camera.Position);
		baseShader.setVector3f("light.light_pos[0]", light_pos[0]);
		baseShader.setVector3f("light.light_pos[1]", light_pos[1]);
		baseShader.setVector3f("light.light_pos[2]", light_pos[2]);
		baseShader.setVector3f("light.light_pos[3]", light_pos[3]);
		baseShader.setVector3f("light.light_color[0]", light_colors[0]);
		baseShader.setVector3f("light.light_color[1]", light_colors[1]);
		baseShader.setVector3f("light.light_color[2]", light_colors[2]);
		baseShader.setVector3f("light.light_color[3]", light_colors[3]);
		dragon.draw(layer);

		baseShader.setVector3f("materialColour", tankColour);
		baseShader.setMatrix4("M", tank_model);
		baseShader.setMatrix4("itM", tank_inverseModel);
		tank.draw(layer);

		baseShader.setVector3f("materialColour", planeColour);
		baseShader.setMatrix4("M", plane_model);
		baseShader.setMatrix4("itM", plane_inverseModel);
		plane.draw(layer);

		baseShader.setVector3f("materialColour", playerColour);
		baseShader.setMatrix4("M", player_model);
		baseShader.setMatrix4("itM", player_inverseModel);
		player.draw(layer);
		};
	int numberOfWindows = 3;
	while (!glfwWindowShouldClose(window[0]) && !glfwWindowShouldClose(window[1]) && !glfwWindowShouldClose(window[2])) {

		double now = glfwGetTime();
		double deltaTime = now - prev;
		deltaFrame++;
		if (deltaTime > 2.0) {
			for (int i = 0; i < numberOfWindows; i++) {
				processInput(window[i]);
			}
			prev = now;
			const double msCount = (double) deltaTime/ deltaFrame;
			deltaFrame = 0;
			std::cout << "\r render time per frame (in seconds): " << msCount;
			std::cout.flush();
		}

		initViewpoint();

		for (int layer = 0; layer < sceneRenders ; layer++) {
			glfwMakeContextCurrent(window[layer]);
			glEnable(GL_DEPTH_TEST);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			if (layer == 1 || layer == 0) {
				glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			}
			else {
				glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
			}
			renderScene(layer);
			glfwSwapBuffers(window[layer]);
		}
		
		glfwPollEvents();
	}

	//clean up ressource
	glfwDestroyWindow(window[0]);
	glfwDestroyWindow(window[1]);
	glfwDestroyWindow(window[2]);
	glfwTerminate();

	return 0;
}


void processInput(GLFWwindow* window) {
	
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(LEFT, 0.1);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(RIGHT, 0.1);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(FORWARD, 0.1);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(BACKWARD, 0.1);

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(1, 0.0, 1);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(-1, 0.0, 1);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, 1.0, 1);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, -1.0, 1);
}


