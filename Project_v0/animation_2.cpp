#include<iostream>

#define GLAD_GL_IMPLEMENTATION
#include<glad/glad.h>
#define GLFW_INCLUDE_NONE
#include<GLFW/glfw3.h>

#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "math.h"
#include <vector>
#include <chrono>
#include <thread>
#include <map>

#include "objects/object.h"
#include "shaders/shader.h"
#include "camera.h"

GLuint compileShader(std::string shaderCode, GLenum shaderType);
GLuint compileProgram(GLuint vertexShader, GLuint fragmentShader);
void processInput(GLFWwindow* window);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void psnrOutput(GLubyte* I, GLubyte* K1, GLubyte* K2);


#ifndef NDEBUG
void APIENTRY glDebugOutput(GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei length,
	const char* message,
	const void* userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << std::endl;
}
#endif

Camera camera(glm::vec3(0.0, 2.0, 3.0));
const int width = 960;
const int height = 513;

int main(int argc, char* argv[]) {
	if (!glfwInit()) {
		throw std::runtime_error("Failed to initialise GLFW \n");
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifndef NDEBUG
	//create a debug context to help with Debugging
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif
	//Create the window
	std::vector<GLFWwindow*> window;
	int numberOfWindows = 3;
	window.resize(numberOfWindows);
	window[0] = glfwCreateWindow(width, height, "Real Time Multi-layer Rendering", NULL, NULL);
	if (window[0] == NULL)
	{
		glfwTerminate();
		throw std::runtime_error("Failed to create GLFW window\n");
	}
	glfwMakeContextCurrent(window[0]);
	gladLoadGL();
	glEnable(GL_DEPTH_TEST);

	for (int layer = 1; layer < numberOfWindows; layer++) {

		window[layer] = glfwCreateWindow(width, height, "Real Time Multi-layer Rendering", NULL, window[0]);
		if (window[layer] == NULL)
		{
			glfwTerminate();
			throw std::runtime_error("Failed to create GLFW window\n");
		}
		glfwMakeContextCurrent(window[layer]);
		gladLoadGL();
		glEnable(GL_DEPTH_TEST);
	}

	//Load OpenGL functions
#ifndef NDEBUG
	int flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		
		glDebugMessageCallback(glDebugOutput, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
#endif
	//Load all shaders and objects used for the scene and computations
	//Shaders for scene objects
	char fileVert[128] = PATH_TO_SHADERS "/base.vert";
	char layer1Frag[128] = PATH_TO_SHADERS "/base1.frag";
	Shader layer1Shader(fileVert, layer1Frag);
	char layer2Frag[128] = PATH_TO_SHADERS "/base2.frag";
	Shader layer2Shader(fileVert, layer2Frag);
	char layer3Frag[128] = PATH_TO_SHADERS "/base3.frag";
	Shader layer3Shader(fileVert, layer3Frag);

	char path_dragon[] = PATH_TO_OBJECTS "/Dragon.obj";
	Object dragon(path_dragon);

	char path_cube[] = PATH_TO_OBJECTS "/tank.obj";
	Object tank(path_cube);

	char path_plane[] = PATH_TO_OBJECTS "/plane.obj";
	Object plane(path_plane);

	char path_player[] = PATH_TO_OBJECTS "/kriegsman.obj";
	Object player(path_player);

	//Init the objects that will be rendered in the scene
	int sceneRenders = 3;
	plane.init(sceneRenders);
	dragon.init(sceneRenders);
	player.init(sceneRenders);
	tank.init(sceneRenders);
	for (int i = 0; i < sceneRenders; i++) {
		if (i == 1) {
			glfwMakeContextCurrent(window[i]);
			dragon.makeObject(layer2Shader, false, i);
			tank.makeObject(layer2Shader, false, i);
			plane.makeObject(layer2Shader, false, i);
			player.makeObject(layer2Shader, false, i);
		}
		else if (i == 2) {
			glfwMakeContextCurrent(window[i]);
			dragon.makeObject(layer3Shader, false, i);
			tank.makeObject(layer3Shader, false, i);
			player.makeObject(layer3Shader, false, i);
			plane.makeObject(layer3Shader, false, i);
		}
		else {
			glfwMakeContextCurrent(window[i]);
			dragon.makeObject(layer1Shader, false, i);
			tank.makeObject(layer1Shader, false, i);
			player.makeObject(layer1Shader, false, i);
			plane.makeObject(layer1Shader, false, i);
		}
	}

	double prev = 0;
	int deltaFrame = 0;

	glm::vec3 light_pos[] = {
		glm::vec3(3.0,3.0,-4.0),
		glm::vec3(0.0,0.0,4.0),
		glm::vec3(3.0,3.0,4.0),
		glm::vec3(2.5, 2.0, -1.0)
	};

	glm::vec3 light_colors[] = {
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 1.0, 1.0)
	};

	glm::mat4 dragon_model = glm::mat4(1.0);
	glm::vec3 dragon_position = glm::vec3(-1.0, 0.0, -1.0);
	glm::vec3 dragonColour = glm::vec3(0.7, 0.1, 0.2);
	dragon_model = glm::translate(dragon_model, dragon_position);
	dragon_model = glm::scale(dragon_model, glm::vec3(0.15, 0.15, 0.15));
	dragon_model = glm::rotate(dragon_model, glm::radians((float)(45.0)), glm::vec3(0.0, 1.0, 0.0));
	glm::mat4 dragon_inverseModel = glm::transpose(glm::inverse(dragon_model));

	glm::mat4 tank_model = glm::mat4(1.0);
	glm::vec3 tankColour = glm::vec3(0.0, 0.4, 0.2);
	glm::vec3 tank_position = glm::vec3(6.0, 0.0, -7.0);
	tank_model = glm::translate(tank_model, tank_position);
	tank_model = glm::scale(tank_model, glm::vec3(0.01, 0.01, 0.01));
	tank_model = glm::rotate(tank_model, glm::radians((float)(-45.0)), glm::vec3(0.0, 1.0, 0.0));
	glm::mat4 tank_inverseModel = glm::transpose(glm::inverse(tank_model));

	glm::mat4 plane_model = glm::mat4(1.0);
	glm::vec3 planeColour = glm::vec3(0.3, 0.3, 0.3);
	glm::vec3 plane_position = glm::vec3(0.0, 0.0, 0.0);
	plane_model = glm::translate(plane_model, plane_position);
	plane_model = glm::scale(plane_model, glm::vec3(100.0, 1.0, 100.0));
	glm::mat4 plane_inverseModel = glm::transpose(glm::inverse(plane_model));

	glm::mat4 player_model = glm::mat4(1.0);
	glm::vec3 playerColour = glm::vec3(0.0, 0.03, 0.3);
	glm::vec3 player_position = glm::vec3(2.5, 0.0, -2.0);
	player_model = glm::translate(player_model, player_position);
	player_model = glm::scale(player_model, glm::vec3(1.0, 1.0, 1.0));
	player_model = glm::rotate(player_model, glm::radians((float)(-15.0)), glm::vec3(0.0, 1.0, 0.0));
	glm::mat4 player_inverseModel = glm::transpose(glm::inverse(player_model));

	glm::mat4 view;
	std::vector<glm::mat4> perspective;

	perspective.resize(4);
	view = camera.GetViewMatrix();
	perspective[0] = camera.GetProjectionMatrix();
	perspective[1] = camera.GetProjectionMatrix();
	perspective[2] = camera.GetProjectionMatrix();
	perspective[3] = camera.GetProjectionMatrix();

	float ambient = 0.2;
	float diffuse = 0.4;
	float specular = 0.4;

	float fov = 45.0;
	float ratio = 1.8;
	float minDist = 0.01;
	float maxDist = 100.0;

	glm::vec3 materialColour = glm::vec3(0.5, 0.5, 0.5);
	//Rendering
	layer1Shader.use();
	layer1Shader.setFloat("shininess", 32.0f);
	layer1Shader.setFloat("light.ambient_strength", ambient);
	layer1Shader.setFloat("light.diffuse_strength", diffuse);
	layer1Shader.setFloat("light.specular_strength", specular);
	layer1Shader.setFloat("light.constant", 0.5);
	layer1Shader.setFloat("light.linear", 0.15);
	layer1Shader.setFloat("light.quadratic", 0.03);

	layer2Shader.use();
	layer2Shader.setFloat("shininess", 32.0f);
	layer2Shader.setFloat("light.ambient_strength", ambient);
	layer2Shader.setFloat("light.diffuse_strength", diffuse);
	layer2Shader.setFloat("light.specular_strength", specular);
	layer2Shader.setFloat("light.constant", 0.5);
	layer2Shader.setFloat("light.linear", 0.15);
	layer2Shader.setFloat("light.quadratic", 0.03);

	layer3Shader.use();
	layer3Shader.setFloat("shininess", 32.0f);
	layer3Shader.setFloat("light.ambient_strength", ambient);
	layer3Shader.setFloat("light.diffuse_strength", diffuse);
	layer3Shader.setFloat("light.specular_strength", specular);
	layer3Shader.setFloat("light.constant", 0.5);
	layer3Shader.setFloat("light.linear", 0.15);
	layer3Shader.setFloat("light.quadratic", 0.03);

	auto initViewpoint = [&]() {
		glm::vec3 position1 = camera.Position;
		float dist_dragon = glm::length(position1 - dragon_position);
		float dist_tank = glm::length(position1 - tank_position);
		float dist_player = glm::length(position1 - player_position);

		float nearest_obj = glm::min(dist_dragon, dist_tank);
		nearest_obj = glm::min(dist_player, nearest_obj);
		float furthest_obj = glm::max(dist_dragon, dist_tank);
		furthest_obj = glm::max(furthest_obj, dist_player);

		view = camera.GetViewMatrix();
		perspective[0] = camera.GetProjectionMatrix(fov, ratio, minDist, maxDist);
		perspective[1] = camera.GetProjectionMatrix(fov, ratio, minDist, maxDist);
		perspective[2] = camera.GetProjectionMatrix(fov, ratio, minDist, maxDist);
		perspective[3] = camera.GetProjectionMatrix(fov, ratio, minDist, maxDist);

		layer1Shader.use();
		layer1Shader.setFloat("nearest_obj", nearest_obj);

		layer2Shader.use();
		layer2Shader.setFloat("nearest_obj", nearest_obj);
		layer2Shader.setFloat("furthest_obj", furthest_obj);

		layer3Shader.use();
		layer3Shader.setFloat("furthest_obj", furthest_obj);
		};

	auto renderScene1 = [&](int layer) {

		layer1Shader.use();
		layer1Shader.setVector3f("materialColour", dragonColour);
		layer1Shader.setMatrix4("M", dragon_model);
		layer1Shader.setMatrix4("itM", dragon_inverseModel);
		layer1Shader.setMatrix4("V", view);
		layer1Shader.setMatrix4("P", perspective[layer]);
		layer1Shader.setVector3f("u_view_pos", camera.Position);
		layer1Shader.setVector3f("light.light_pos[0]", light_pos[0]);
		layer1Shader.setVector3f("light.light_pos[1]", light_pos[1]);
		layer1Shader.setVector3f("light.light_pos[2]", light_pos[2]);
		layer1Shader.setVector3f("light.light_pos[3]", light_pos[3]);
		layer1Shader.setVector3f("light.light_color[0]", light_colors[0]);
		layer1Shader.setVector3f("light.light_color[1]", light_colors[1]);
		layer1Shader.setVector3f("light.light_color[2]", light_colors[2]);
		layer1Shader.setVector3f("light.light_color[3]", light_colors[3]);

		dragon.draw(layer);

		layer1Shader.setVector3f("materialColour", tankColour);
		layer1Shader.setMatrix4("M", tank_model);
		layer1Shader.setMatrix4("itM", tank_inverseModel);
		tank.draw(layer);

		layer1Shader.setVector3f("materialColour", planeColour);
		layer1Shader.setMatrix4("M", plane_model);
		layer1Shader.setMatrix4("itM", plane_inverseModel);
		plane.draw(layer);

		layer1Shader.setVector3f("materialColour", playerColour);
		layer1Shader.setMatrix4("M", player_model);
		layer1Shader.setMatrix4("itM", player_inverseModel);
		player.draw(layer);
		};

	auto renderScene2 = [&](int layer) {

		layer2Shader.use();
		layer2Shader.setVector3f("materialColour", dragonColour);
		layer2Shader.setMatrix4("M", dragon_model);
		layer2Shader.setMatrix4("itM", dragon_inverseModel);
		layer2Shader.setMatrix4("V", view);
		layer2Shader.setMatrix4("P", perspective[layer]);
		layer2Shader.setVector3f("u_view_pos", camera.Position);
		layer2Shader.setVector3f("light.light_pos[0]", light_pos[0]);
		layer2Shader.setVector3f("light.light_pos[1]", light_pos[1]);
		layer2Shader.setVector3f("light.light_pos[2]", light_pos[2]);
		layer2Shader.setVector3f("light.light_pos[3]", light_pos[3]);
		layer2Shader.setVector3f("light.light_color[0]", light_colors[0]);
		layer2Shader.setVector3f("light.light_color[1]", light_colors[1]);
		layer2Shader.setVector3f("light.light_color[2]", light_colors[2]);
		layer2Shader.setVector3f("light.light_color[3]", light_colors[3]);

		dragon.draw(layer);

		layer2Shader.setVector3f("materialColour", tankColour);
		layer2Shader.setMatrix4("M", tank_model);
		layer2Shader.setMatrix4("itM", tank_inverseModel);
		tank.draw(layer);

		layer2Shader.setVector3f("materialColour", planeColour);
		layer2Shader.setMatrix4("M", plane_model);
		layer2Shader.setMatrix4("itM", plane_inverseModel);
		plane.draw(layer);

		layer2Shader.setVector3f("materialColour", playerColour);
		layer2Shader.setMatrix4("M", player_model);
		layer2Shader.setMatrix4("itM", player_inverseModel);
		player.draw(layer);
		};

	auto renderScene3 = [&](int layer) {
		layer3Shader.use();
		layer3Shader.setVector3f("materialColour", dragonColour);
		layer3Shader.setMatrix4("M", dragon_model);
		layer3Shader.setMatrix4("itM", dragon_inverseModel);
		layer3Shader.setMatrix4("V", view);
		layer3Shader.setMatrix4("P", perspective[layer]);
		layer3Shader.setVector3f("u_view_pos", camera.Position);
		layer3Shader.setVector3f("light.light_pos[0]", light_pos[0]);
		layer3Shader.setVector3f("light.light_pos[1]", light_pos[1]);
		layer3Shader.setVector3f("light.light_pos[2]", light_pos[2]);
		layer3Shader.setVector3f("light.light_pos[3]", light_pos[3]);
		layer3Shader.setVector3f("light.light_color[0]", light_colors[0]);
		layer3Shader.setVector3f("light.light_color[1]", light_colors[1]);
		layer3Shader.setVector3f("light.light_color[2]", light_colors[2]);
		layer3Shader.setVector3f("light.light_color[3]", light_colors[3]);
		dragon.draw(layer);

		layer3Shader.setVector3f("materialColour", tankColour);
		layer3Shader.setMatrix4("M", tank_model);
		layer3Shader.setMatrix4("itM", tank_inverseModel);
		tank.draw(layer);

		layer3Shader.setVector3f("materialColour", planeColour);
		layer3Shader.setMatrix4("M", plane_model);
		layer3Shader.setMatrix4("itM", plane_inverseModel);
		plane.draw(layer);

		layer3Shader.setVector3f("materialColour", playerColour);
		layer3Shader.setMatrix4("M", player_model);
		layer3Shader.setMatrix4("itM", player_inverseModel);
		player.draw(layer);
		};

	int frameCounter = 1;

	glfwSwapInterval(1);
	while (!glfwWindowShouldClose(window[0]) && !glfwWindowShouldClose(window[1]) && !glfwWindowShouldClose(window[2])) {
		glfwPollEvents();

		double now = glfwGetTime();
		double deltaTime = now - prev;
		deltaFrame++;
		if (deltaTime > 2.0) {
			for (int i = 0; i < numberOfWindows; i++) {
				processInput(window[i]);
			}
			prev = now;
			const double msCount = (double)deltaTime / deltaFrame;
			deltaFrame = 0;
			std::cout << "\r render time per frame (in seconds): " << msCount;
			std::cout.flush();
		}

		initViewpoint();

		for (int layer = 0; layer < sceneRenders; layer++) {
			glfwMakeContextCurrent(window[layer]);
			glEnable(GL_DEPTH_TEST);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			if (layer == 1 || layer == 0) {
				glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			}
			else {
				glClearColor(0.7f, 0.7f, 0.7f, 0.7f);
			}
			if (layer == 0) { 
				renderScene1(layer); 
			} else if (layer == 1) { 
				renderScene2(layer); 
			}
			else if (layer == 2) {
				renderScene3(layer);
			}
			glfwSwapBuffers(window[layer]);

		}
	}
	glfwDestroyWindow(window[0]);
	glfwDestroyWindow(window[1]);
	glfwDestroyWindow(window[2]);
	glfwTerminate();

	return 0;
}

void processInput(GLFWwindow* window) {
	/*
	Function to process input: zqsd to move and keyboard arrows to look around
	*/
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(LEFT, 0.1);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(RIGHT, 0.1);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(FORWARD, 0.1);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(BACKWARD, 0.1);

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(1, 0.0, 1);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(-1, 0.0, 1);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, 1.0, 1);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, -1.0, 1);
}