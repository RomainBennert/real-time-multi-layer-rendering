#version 430 core
out vec4 FragColor;
  
in vec2 TexCoords;

uniform int width;
uniform sampler2D screen1;
uniform sampler2D screen2;
uniform sampler2D screen3;


void main()
{ 
    float pixelsPerCm = 40.0/width;
    float distToScreen = 100.0; // How far is the viewer from the screen [cm]
    float distBetweenScreens = 0.8; // How far apart the screens are from each other [cm]
    float posOffset = 20.0; // How far to the left the viewer is compared to the center [cm]
    float pixelOffset = ((posOffset*distBetweenScreens)/distToScreen)*pixelsPerCm; // Offset between textures in pixels
    vec2 coord1 = vec2(TexCoords.x - pixelOffset, TexCoords.y);
    vec2 coord3 = vec2(TexCoords.x + pixelOffset , TexCoords.y);
    vec4 colorScreen1, colorScreen3;
    if (coord1.x >= 0) {
        colorScreen1 = texture(screen1, coord1);
    } else {
        colorScreen1 = vec4(0.0,0.0,0.0,1.0);
    }
    if (coord3.x <= 1) {
        colorScreen3 = texture(screen3, coord3);
    } else {
        colorScreen3 = vec4(0.0,0.0,0.0,1.0);
    }

    FragColor = (colorScreen3*texture(screen2, TexCoords))*colorScreen1;
}