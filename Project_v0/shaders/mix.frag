#version 430 core
out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2D screen1;
uniform sampler2D screen2;
uniform sampler2D screen3;


void main()
{ 

    FragColor = (texture(screen3, TexCoords)*texture(screen2, TexCoords))*texture(screen1, TexCoords);
}