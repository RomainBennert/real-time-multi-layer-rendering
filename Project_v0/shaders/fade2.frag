#version 430 core
out vec4 FragColor;
precision mediump float;
in vec3 v_frag_coord;
in vec3 v_normal;
uniform vec3 u_view_pos;

struct Light{
	vec3 light_pos[4];
	vec3 light_color[4];
	float ambient_strength;
	float diffuse_strength;
	float specular_strength;
	float constant;
	float linear;
	float quadratic;
};

uniform Light light;
uniform float shininess;
uniform vec3 materialColour;
uniform float nearest_obj;
uniform float furthest_obj;
uniform float fadeLen;

float specularCalculation(vec3 N, vec3 L, vec3 V ){
	vec3 R = reflect (-L,N);
	float cosTheta = dot(R , V);
	float spec = pow(max(cosTheta,0.0), 32.0);
	return light.specular_strength * spec;
}

void main() {
	float distToFrag = length(u_view_pos - v_frag_coord);
	if (distToFrag + fadeLen < nearest_obj || distToFrag - fadeLen >= furthest_obj) {
		FragColor = vec4(1.0, 1.0, 1.0, 1.0);
	} else if (distToFrag - fadeLen <= nearest_obj){
		float l = 0 ;
		float fade = ((nearest_obj + fadeLen) - distToFrag)/(fadeLen*2.0);
		vec3 white = vec3(1.0,1.0,1.0);
		vec3 color = vec3(1.0,1.0,1.0);
		for (int i = 0; i<4; ++i){
			vec3 N = normalize(v_normal);
			vec3 L = normalize(light.light_pos[i] - v_frag_coord) ;
			vec3 V = normalize(u_view_pos - v_frag_coord);
			float specular = specularCalculation( N, L, V);
			float diffuse = light.diffuse_strength * max(dot(N,L),0.0);
			float distance = length(light.light_pos[i] - v_frag_coord);
			float attenuation = 1 / (light.constant + light.linear * distance + light.quadratic * distance * distance);
			l += attenuation * (diffuse + specular);
			color += attenuation * (diffuse + specular)*light.light_color[i].xyz;
		}
		float light = light.ambient_strength + l;
		FragColor = vec4(materialColour * vec3(light) * color.xyz + fade*white.xyz, 1.0);
	} else if (distToFrag + fadeLen >= furthest_obj) {
		float l = 0 ;
		float fade = ((furthest_obj - fadeLen) - distToFrag)/(fadeLen*2.0);
		vec3 white = vec3(1.0,1.0,1.0);
		vec3 color = vec3(1.0,1.0,1.0);
		for (int i = 0; i<4; ++i){
			vec3 N = normalize(v_normal);
			vec3 L = normalize(light.light_pos[i] - v_frag_coord) ;
			vec3 V = normalize(u_view_pos - v_frag_coord);
			float specular = specularCalculation( N, L, V);
			float diffuse = light.diffuse_strength * max(dot(N,L),0.0);
			float distance = length(light.light_pos[i] - v_frag_coord);
			float attenuation = 1 / (light.constant + light.linear * distance + light.quadratic * distance * distance);
			l += attenuation * (diffuse + specular);
			color += attenuation * (diffuse + specular)*light.light_color[i].xyz;
		}
		float light = light.ambient_strength + l;
		FragColor = vec4(materialColour * vec3(light) * color.xyz - fade*white.xyz, 1.0);
	} else {
		float l = 0 ;
		vec3 color = vec3(1.0,1.0,1.0);
		for (int i = 0; i<4; ++i){
			vec3 N = normalize(v_normal);
			vec3 L = normalize(light.light_pos[i] - v_frag_coord) ;
			vec3 V = normalize(u_view_pos - v_frag_coord);
			float specular = specularCalculation( N, L, V);
			float diffuse = light.diffuse_strength * max(dot(N,L),0.0);
			float distance = length(light.light_pos[i] - v_frag_coord);
			float attenuation = 1 / (light.constant + light.linear * distance + light.quadratic * distance * distance);
			l += attenuation * (diffuse + specular);
			color += attenuation * (diffuse + specular)*light.light_color[i].xyz;
		}
		float light = light.ambient_strength + l;
		FragColor = vec4(materialColour * vec3(light) * color.xyz, 1.0);
	}
}

